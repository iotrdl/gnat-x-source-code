/*
Copyright 2014 Deep Information Sciences, Inc. and the University of New Hampshire (UNH)
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
*
http://www.apache.org/licenses/LICENSE-2.0
*
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*
*
@author Thomas P Kimsey
8/20/13
*
*/


#include "stdlib.h"
#include <device.h>
#include <Cellular.h>
#include <stdio.h>

	
uint8 mem[100];
uint8 temp = 0;
uint8 i = 0;
char str[];
char str1[1];
char str2[2];
char str3[3];
uint8 ready = 0;

uint8 newData = 0;
uint8 dataRec[255];

void Cellular_Start(){
	UART_Start();
	Pin_2_Write(0);
	CyDelay(1000);
	Pin_2_Write(1);
	while( ready != 2){
	
		if(UART_GetRxBufferSize() >0){
			temp = UART_GetByte();
			// received +
			if ( temp == 0x2B ||temp == 0x4F  ){
				i = 0;
			}
			mem[i]= temp;
			
			i++;
		}
		
		//Ready!!!!
		if(mem[0] == 0x2B &&  /*mem[1] == 0x53 &&mem[2] == 0x49 &&mem[3] == 0x4e &&mem[4] == 0x44 &&mem[5] == 0x3a &&mem[6] == 0x20 &&*/ mem[7] == 0x34 ){
			CyDelay(1);
			
			i = 0;
			ready++;
		}
		
		//Sind:11 Good to go
		if(mem[0] == 0x2B && mem[1] == 0x53 &&mem[2] == 0x49 &&mem[3] == 0x4e &&mem[4] == 0x44 &&mem[5] == 0x3a &&mem[6] == 0x20 && mem[7] == 0x31 && mem[8] == 0x31 ){
			CyDelay(1);
			
			i = 0;
			ready++;
		}
		
		//Sind: 7 Action:reset
		if(mem[0] == 0x2B && mem[1] == 0x53 &&mem[2] == 0x49 &&mem[3] == 0x4e &&mem[4] == 0x44 &&mem[5] == 0x3a &&mem[6] == 0x20 && mem[7] == 0x37 ){
			//CyDelay(1);
			Pin_2_Write(0);
			CyDelay(130);
			Pin_2_Write(1);
			i = 0;
			ready=0;
		}
		
	}
	CyDelay(550);
	UART_PutString("AT+SBAND=8");
	UART_PutChar(0x0D);
	UART_PutChar(0x0A);
	
	Cellular_waitForOK();
	CyDelay(550);
	UART_PutString("AT+CGDCONT=1,\"IP\",\"epc.tmobile.com\"");
	UART_PutChar(0x0D);
	UART_PutChar(0x0A);
	
	Cellular_waitForOK();
	CyDelay(550);
	UART_PutString("AT+CGACT=1,1");
	UART_PutChar(0x0D);
	UART_PutChar(0x0A);
	
	Cellular_waitForOK();
	CyDelay(550);
	UART_PutString("AT+SDATACONF=1,\"TCP\",\"unh00.cloudtree.net\",1883");
	UART_PutChar(0x0D);
	UART_PutChar(0x0A);
	
	Cellular_waitForOK();
	CyDelay(50);
	UART_PutString("AT+SDATASTART=1,1");
	UART_PutChar(0x0D);
	UART_PutChar(0x0A);
	CyDelay(500);
	Cellular_waitForOK();
	

}

void Cellular_disconnect(){
	UART_PutString("AT+SDATASTART=1,0");
	UART_PutChar(0x0D);
	UART_PutChar(0x0A);

}


//Sends a packet of data 
void Cellular_sendData( uint8 info[], uint8 length){
	uint8 i = 0;
	int l = length;
	
	
	UART_PutString("AT+SDATATSEND=1," );
	
	if ( length < 10 ){
		sprintf(str1,"%d",l);
		UART_PutString(str1 );
	}
	else if ( length < 100 ){
		sprintf(str2,"%d",l);
		UART_PutString(str2 );
	}
	else {
		sprintf(str3,"%d",l);
		UART_PutString(str3 );
	}
	
	
	UART_PutChar(0x0D);
	UART_PutChar(0x0A);
	CyDelay(500);
	
	while( i < length){
		UART_PutChar(info[i]);
		i++;
	}
	

	UART_ClearRxBuffer();
	
	UART_PutChar(0x1A);
	UART_PutChar(0x0D);
	UART_PutChar(0x0A);
	
	Cellular_waitForOK();
}

//Indicates if there is new data to read
uint8 Cellular_dataReceived(){
	return newData;
}


//Reads in data recieved 
void Cellular_readData(){
	uint8 tempChar;
	uint8 index = 0;
	uint8 bufSize;
	UART_ClearRxBuffer();
	UART_PutString("AT+SDATATREAD=1");
	UART_PutChar(0x0D);
	UART_PutChar(0x0A);
	CyDelay(100);
	
	tempChar = UART_GetByte(); //reads in first char
	
	//Recieved data will be in form:
	//+SDATA: 1, 10,1234567890 (received 10 bytes from remote host, hex format) 
	//we dont need the initial information so we will start saving after 2nd comma.
	while (tempChar != 0x2c){
		tempChar = UART_GetByte();
	}
	tempChar = UART_GetByte();
	while (tempChar != 0x2c){
		tempChar = UART_GetByte();
	}
	bufSize = UART_GetRxBufferSize();
	for( index = 0; index < bufSize; index++){
	
		dataRec[index] = UART_GetByte(); //fills the recData
	}
	
	newData = 0;
}

//Returns the signal level
uint8 Cellular_getSignal(){
	
	
}

//Restarts the SM5100B
void Cellular_restart(){

}

void Cellular_Process(){
	uint8 dataIn[32];
	uint8 index = 0;
	uint8 bufSize;
	uint8 temp;
	uint8 returnHit = 255;
	
	bufSize = UART_GetRxBufferSize();
	while( UART_GetRxBufferSize()>0 && returnHit != 1){ 
		temp = UART_GetByte();
		if (temp == 0x0A || temp == 0x0D ) {
			if(index >0) returnHit = 1;
		}
		else{
			dataIn[index] = temp;
			index++;
			returnHit = 0;
		}
	}
	
	//received data
	//+STCPD: 1 
	if ( dataIn[0] == 0x2b && dataIn[1] == 0x53 && dataIn[2] == 0x54 && dataIn[3] == 0x43 && dataIn[4] == 0x50 && dataIn[5] == 0x44 && dataIn[6] == 0x3a  && dataIn[7] == 0x31 ){
		newData = 1;
		UART_ClearRxBuffer();
	}
	
	else if (1==0){
		
	
	}
	
}

void Cellular_waitForOK(){

uint8 okRec = 0;

	while( okRec ==0 ){
		if(UART_GetRxBufferSize() >0){
			temp = UART_GetByte();
			// received +
			if (temp == 0x4F  ){
				i = 0;
			}
			mem[i]= temp;
			
			i++;
		}
		
		if(mem[0] == 0x4F &&  mem[1] == 0x4B ){
		
			okRec = 1;
			mem[0] = 0;
			mem[1] = 1;
		}
	}
	UART_GetByte();
	UART_GetByte();
	UART_GetByte();
	UART_GetByte();
}

/* [] END OF FILE */