/*
Copyright 2014 Deep Information Sciences, Inc. and the University of New Hampshire (UNH)
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
*
http://www.apache.org/licenses/LICENSE-2.0
*
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*
*
@author Thomas P Kimsey
8/20/13
*
*/

void Cellular_Start(void);
void Cellular_disconnect(void);
void Cellular_waitForOK(void);
void Cellular_sendData( uint8 info[], uint8 length);
uint8 Cellular_dataReceived(void);
void Cellular_readData(void);
void Cellular_Process(void);
uint8 Cellular_getSignal();
void Cellular_restart();
//[] END OF FILE