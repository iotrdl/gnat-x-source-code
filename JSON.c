/*
Copyright 2014 Deep Information Sciences, Inc. and the University of New Hampshire (UNH)
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
*
http://www.apache.org/licenses/LICENSE-2.0
*
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*
*
@author Thomas P Kimsey
8/20/13
*
*/
#include <device.h>
#include <JSON.h>


uint8 JSONPacket[255];
uint8 index = 0;

void JSON_Clear(){
	int i;
	for( i = 0; i < 255; i++){
		JSONPacket[i] = 0;
	}
	index = 0;
}

void JSON_addString(char8 * string){
	
	uint8 length = 0;
	uint8 indexU8 = 1u;
    char8 current = *string;
	

	while(current != (char8) '\0')
    {
        JSONPacket[index] = (current);
        current = *(string + indexU8);
        indexU8++;
		index ++;
    }
}

void JSON_addChar( uint8 char1){
	JSONPacket[index] = char1;
	index++;
}

void JSON_newLine(){
	JSONPacket[index] = 0x0D;
	index++;
	JSONPacket[index] = 0x0A;
	index++;
}

uint8 JSON_getIndex(void){
	return index;

}

/* [] END OF FILE */
