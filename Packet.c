/*
Copyright 2014 Deep Information Sciences, Inc. and the University of New Hampshire (UNH)
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
*
http://www.apache.org/licenses/LICENSE-2.0
*
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*
*
@author Thomas P Kimsey
8/20/13
*
*/

#include <device.h>
#include <Packet.h>
#include <Time.h>
#include <w5100.h>
#include <XBee.h>

extern uint8 dataRec[255];
uint8 packet[255];
uint8 modeOp;
uint16 id;
void Packet_Start(uint8 mode, uint16 idNum){
	modeOp = mode;
	id = idNum;

}

void Packet_Read(){
	uint8 i = 0;
	uint8 received = 0;
	uint8 prev = 0;
	uint8 curr = 0;
	i=Xbee_GetRxBufferSize();
	//Wired
	if ( (modeOp == 1 || modeOp == 3 )&& W5100_dataReceived() == 0x01 ){
		W5100_readUDP();
		received = 1;
		if( received == 1 ) Packet_Process();
	}	
	
	//wireless
	
	else if ((modeOp == 2 || modeOp == 3 ) && Xbee_GetRxBufferSize() >= 11 ){
		while( curr != 0xFF || prev == 0xFE ){
			prev = curr;
			curr = Xbee_Read();
		}
		while (Xbee_GetRxBufferSize() < 10){}
		
		received = 0;
		i =0;
		packet[0]=Xbee_Read();
		packet[1]=Xbee_Read();
		packet[2]=Xbee_Read();
		packet[3]=Xbee_Read();
		packet[4]=Xbee_Read();
		packet[5]=Xbee_Read();
		packet[6]=Xbee_Read();
		packet[7]=Xbee_Read();
		packet[8]=Xbee_Read();
		packet[9]=Xbee_Read();
		if (packet[9] > 20) return;
		while(Xbee_GetRxBufferSize() < packet[9]){
			CyDelay(1);
		}
		for(i = 0;i < packet[9];i++){
			packet[10+i] = Xbee_Read();
		}
		
		received = 1;
		if( received == 1 ) Packet_ProcessWireless();
	}
	
	 

}

void Packet_ProcessWireless(){
		//Connect Ack
	if( packet[0] == 1){
	
	}
	
	//Disconnect Ack
	else if( packet[0] == 3){
	
	}	

	//ping request
	else if( packet[0] == 5){
		uint8 sendBuffer[255];
		if (modeOp == 1){
			sendBuffer[0] = 6;                    //ping response type
			sendBuffer[1] = id >> 8;              //first id byte
			sendBuffer[2] = id & 0x00FF;          //second id byte
			sendBuffer[3] = Time_getTime0();      //time
			sendBuffer[4] = Time_getTime1();
			sendBuffer[5] = Time_getTime2();
			sendBuffer[6] = Time_getTime3();
			sendBuffer[7] = Time_getTime4();
			sendBuffer[8] = Time_getTime5();
			//W5100_sendUDP(sendBuffer, 9);         //Sends ping response over udp

		}
	}
	
	//Data Request
	else if( packet[0] == 7){
	
	}
	
	//Data Response
	//From Xbee unit --> Server fowarding
	else if( packet[0] == 8){
		if( modeOp == 3){
			W5100_send(packet, 10+packet[9] ); 
		}
	}

	//Time Request
	else if( packet[0] == 9){
		Packet_sendTime();
	}
	
	//Time Response
	else if( packet[0] == 10){
		Time_setTime(packet[3], packet[4], packet[5], packet[6], packet[7], packet[8]);
		CyDelay(1);
	}	
}


void Packet_Process(){
	
	//Connect Ack
	if( dataRec[0] == 1){
	
	}
	
	//Disconnect Ack
	else if( dataRec[0] == 3){
	
	}	

	//ping request
	else if( dataRec[0] == 5){
		uint8 sendBuffer[255];
		if (modeOp == 1){
			sendBuffer[0] = 6;                    //ping response type
			sendBuffer[1] = id >> 8;              //first id byte
			sendBuffer[2] = id & 0x00FF;          //second id byte
			sendBuffer[3] = Time_getTime0();      //time
			sendBuffer[4] = Time_getTime1();
			sendBuffer[5] = Time_getTime2();
			sendBuffer[6] = Time_getTime3();
			sendBuffer[7] = Time_getTime4();
			sendBuffer[8] = Time_getTime5();
			W5100_send(sendBuffer, 9);         //Sends ping response over udp

		}
	}
	
	//Data Request
	else if( dataRec[0] == 7){
	
	}
	
	//Data Response
	//From Xbee unit --> Server fowarding
	else if( dataRec[0] == 8){
		if( modeOp == 3){
			
		}
	}

	//Time Request
	else if( dataRec[0] == 9){
		Packet_sendTime();
	}
	
	//Time Response
	else if( dataRec[0] == 10){
		Time_setTime(packet[4], packet[5], packet[6], packet[7], packet[8], packet[9]);
		CyDelay(1);
	}
}


//Establishes a connection to a server
uint8 Packet_Connect(void){
	uint8 sendBuffer[255];
	if (modeOp == 1 || modeOp ==3){
		sendBuffer[0] = 1;
		sendBuffer[1] = id >> 8;
		sendBuffer[2] = id & 0x00FF;
		sendBuffer[3] = Time_getTime0();
		sendBuffer[4] = Time_getTime1();
		sendBuffer[5] = Time_getTime2();
		sendBuffer[6] = Time_getTime3();
		sendBuffer[7] = Time_getTime4();
		sendBuffer[8] = Time_getTime5();
		W5100_send(sendBuffer, 9);

		while( W5100_dataReceived() == 0x00){
	
		}
		
		W5100_readUDP();
		if ( dataRec[0] == 2 ){
			return 0x01;
		}
		else{
			return 0x00;
		}
		
	}
	
	return 0x01;
	
}

//Disconnects from the server
void Packet_Disconnect(void){
	uint8 sendBuffer[255];
	if (modeOp == 1){
		sendBuffer[0] = 3;
		sendBuffer[1] = id >> 8;
		sendBuffer[2] = id & 0x00FF;
		sendBuffer[3] = Time_getTime0();
		sendBuffer[4] = Time_getTime1();
		sendBuffer[5] = Time_getTime2();
		sendBuffer[6] = Time_getTime3();
		sendBuffer[7] = Time_getTime4();
		sendBuffer[8] = Time_getTime5();
		W5100_send(sendBuffer, 9);
	}
}

//Sends a packet of sensor data
void Packet_sendData(uint8 sensorNum, uint8 dout[], uint8 length){
	uint8 sendBuffer[255];
	uint8 i = 0;
	uint8 j= 0;  //index for xbee sending
	if (modeOp == 1){
		sendBuffer[0] = 8;
		sendBuffer[1] = id >> 8;
		sendBuffer[2] = id & 0x00FF;
		sendBuffer[3] = Time_getTime0();
		sendBuffer[4] = Time_getTime1();
		sendBuffer[5] = Time_getTime2();
		sendBuffer[6] = Time_getTime3();
		sendBuffer[7] = Time_getTime4();
		sendBuffer[8] = Time_getTime5();
		sendBuffer[9] = length +1;
		sendBuffer[10] = sensorNum;
		for ( i = 0; i < length; i++){
		
			sendBuffer[11+i] = dout[i];
		}
		W5100_send(sendBuffer, 11 +length);
		
	}
	else if ( modeOp == 2){
		Xbee_WriteSpecial();  //special header start
		Xbee_Write(8);
		Xbee_Write(id >> 8);
		Xbee_Write(id & 0x00FF);
		//Xbee_Write(0);
		//Xbee_Write(0);
		//Xbee_Write(0);
		//Xbee_Write(0);
		//Xbee_Write(0);
		//Xbee_Write(0);
		
		
		Xbee_Write(Time_getTime0());
		Xbee_Write(Time_getTime1());
		Xbee_Write(Time_getTime2());
		Xbee_Write(Time_getTime3());
		Xbee_Write(Time_getTime4());
		Xbee_Write(Time_getTime5());
		
		Xbee_Write(length+1);
		Xbee_Write(sensorNum);
		for ( i = 0; i < length; i++){
			
			Xbee_Write(dout[i]);
			
		}
		 
		
	}
	
}

void Packet_sendTimeReq(){
	
	if( modeOp == 2 ){
		Xbee_WriteSpecial();
		Xbee_Write(9);
		Xbee_Write(id >> 8);
		Xbee_Write(id & 0x00FF);
		Xbee_Write(0x00); //Time0
		Xbee_Write(0x00);
		Xbee_Write(0x00);
		Xbee_Write(0x00);
		Xbee_Write(0x00);
		Xbee_Write(0x00); //Time5
		Xbee_Write(0x00); //Size

	}
}

void Packet_sendTime(){
	
	if (modeOp == 3){ 
		Xbee_WriteSpecial();
		Xbee_Write(10);
		Xbee_Write(id >> 8);  // id0
		Xbee_Write(id & 0x00FF);// id1
		Xbee_Write(Time_getTime0());
		Xbee_Write(Time_getTime1());
		Xbee_Write(Time_getTime2());
		Xbee_Write(Time_getTime3());
		Xbee_Write(Time_getTime4());
		Xbee_Write(Time_getTime5());
		Xbee_Write(0x00); //size
	}
}


/* [] END OF FILE */
