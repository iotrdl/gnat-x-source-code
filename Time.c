/*
Copyright 2014 Deep Information Sciences, Inc. and the University of New Hampshire (UNH)
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
*
http://www.apache.org/licenses/LICENSE-2.0
*
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*
*
@author Thomas P Kimsey
8/20/13
*
*
* A custom real time clock component
*
* Keeps track of the time in 32 bit unix timestamp 
* format as well as keeping millisecond accuracy 
* with an extra 16 bits if needed.  
*
* The clock is initialized and synced with a 
* NIST NTP time server for accurate time calibration
*
*/
#include <device.h>
#include <W5100_1.h>
#include <Time.h>

extern uint8 dataRec_1[255];
uint8 packetT[255];
uint8 network = 0;
uint32 secsSince1900;
uint32 word1;
uint32 word2;
uint32 word3;
uint32 word4;
uint32 word5;
uint32 word6;
const unsigned long seventyYears = 2208988800; 
	
	
// Initalizes the real time clock	
void Time_Start(uint8 mode){
	
	uint16 j = 0;
	network = mode;
	
	for( j = 0; j < 255; j++){
		packetT[j] = 0;
	}
	Timer_1_Start();
	Timer_2_Start();
	if( network == 1 || network == 3 ){
		W5100_1_Start();
		//ntp server detailas
		W5100_1_setPort( 0x00, 0x7B);  
		W5100_1_setIp( 132, 163, 4, 101);
	}
	isr_1_Start();
	milliISR_Start();
	TimeLock_Start();
	if( network  == 1 || network == 3 ){
		Time_sync();
		lastSync = currentTime;
	}


	

	
}

// Syncronizes the onboard time
// with a nist ntp time server.
void Time_sync(){
	
	uint8 port0;
	uint8 port1;
	uint8 ip0;
	uint8 ip1;
	uint8 ip2;
	uint8 ip3;
	uint16 timeEnd;
	uint16 timeDelay;
	uint16 millis = 0;
	uint16 timerCount = 0;
	previousTime = currentTime;
	previousMillis = milliSecond;
	milliSecond = 0;
	
		
	port0 = W5100_1_readPort0();
	port1 = W5100_1_readPort1();
	
	ip0 = W5100_1_readIp0();
	ip1 = W5100_1_readIp1();
	ip2 = W5100_1_readIp2();
	ip3 = W5100_1_readIp3();

	
	
	
	
	packetT[0] = 0xE3;   // LI, Version, Mode
	packetT[1] = 0;     // Stratum, or type of clock
	packetT[2] = 6;     // Polling Interval
	packetT[3] = 0xEC;  // Peer Clock Precision
	// 8 bytes of zero for Root Delay & Root Dispersion
	packetT[12]  = 49;
	packetT[13]  = 0x4E;
	packetT[14]  = 49;
	packetT[15]  = 52;
	milliSecond = 0;
	Timer_2_WriteCounter(24000);
	//timeStart = milliSecond;
	W5100_1_sendUDP( packetT, 48 );
	while ( !W5100_1_dataReceived() && milliSecond < 999 )
	{	
		CR_Write(0x03);
		CyDelayUs(1);
		CR_Write(0x00);
	}
	
	
	if( milliSecond < 999){
		W5100_1_readUDP();
		timeEnd = milliSecond;	
		timeDelay = (timeEnd)/2;
		milliSecond = 0;
		Timer_2_WriteCounter(24000);
		
		
		
		
		
		word1 = dataRec_1[40];
		word1 = word1 << 24;
		word2 = dataRec_1[41];
		word2 = word2 << 16;
		word3 = dataRec_1[42];
		word3 = word3<< 8;
		word4 = dataRec_1[43];
		word5 = dataRec_1[44];
		word6 = dataRec_1[45];
		if(word5 & 0x80) millis+=500; 
		if(word5 & 0x40) millis+=250;
		if(word5 & 0x20) millis+=125; 
		if(word5 & 0x10) millis+=63; 
		if(word5 & 0x08) millis+=31;
		if(word5 & 0x04) millis+=16; 
		if(word5 & 0x02) millis+=8; 
		if(word5 & 0x01) millis+=4; 
		if(word6 & 0x80) millis+=2;
		if(word6 & 0x40) millis+=1; 
	    
		
		// combine the four bytes (two words) into a long integer
	    // this is NTP time (seconds since Jan 1 1900):
	   	secsSince1900 = (word1) | (word2) | (word3) | (word4);  
	     
		
		currentTime = secsSince1900 - seventyYears;
		milliSecond = milliSecond+millis+timeDelay;
		timerCount = 32768-(33*milliSecond);
		if( timerCount > 32768 ) timerCount = 1;
		Timer_1_WriteCounter(timerCount);
		lastSync = currentTime;
		//W5100_1_setPort(0, 26);  
		//W5100_1_setIp( 132,177,206,176 );
	}
	else {
	//must be modified to fall to right time
		lastSync = currentTime;
		currentTime++;
	}
}


// Increments the second counter
// resyncronizes if enough time has passed
void Time_addSecond(){
	

	currentTime++;
	milliSecond = 0;

	updated = 1;
}

// Increments the millisecond counter
void Time_addMilliSecond(){
	if( milliSecond <999){
		milliSecond ++;
	}
	updated = 1;
}

// returns a unix 32 bit timestamp
uint32 Time_getTime(){
	if( currentTime < previousTime){
		return previousTime;
	}
	return currentTimeLock;
}

// Returns the first byte of a unix 32 bit timestamp
uint8 Time_getTime0(){
	if (currentTime - lastSync > 60000 ) Time_sync();
	updated = 0;
	CR_Write(0x04);
	CR_Write(0x00);
	return (Time_getTime()>> 24);
	
}

// Returns the second byte of a unix 32 bit timestamp
uint8 Time_getTime1(){
	return (Time_getTime() >> 16);
}

// Returns the third byte of a unix 32 bit timestamp
uint8 Time_getTime2(){
	return (Time_getTime() >> 8);
}

// Returns the fourth byte of a unix 32 bit timestamp
uint8 Time_getTime3(){
	return (Time_getTime() & 0xFF);
}
uint16 Time_getMilliseconds(){
	
	if( currentTimeLock < previousTime){
		if( milliSecondLock < previousMillis){
			return previousMillis;
		}
		else{
			return milliSecondLock;
		}
	}
	if( currentTime == previousTime && milliSecond < previousMillis){
			return previousMillis;
	}
	return milliSecondLock;
}
// Returns the first byte of the current system millisecond counter
uint8 Time_getTime4(){
	return (Time_getMilliseconds() >> 8 );
}

// Returns the second byte of the current system millisecond counter
uint8 Time_getTime5(){
	return (Time_getMilliseconds() & 0xFF);
}

// returns 0x01 if there has been a new clock tick 
// since the time has been last read, 0x00 if not
uint8 Time_isUpdated(){
	return updated;
}

void Time_setTime(uint32 t0, uint32 t1, uint32 t2, uint32 t3, uint32 t4, uint32 t5){
	uint32 newSeconds = 0;
	uint32 newMilli = 0;
	newSeconds+=t0 << 24;
	newSeconds+= t1 << 16;
	newSeconds+= t2 << 8;
	newSeconds+= t3;
	newMilli +=t4 << 8;
	newMilli +=t5;
	currentTime = newSeconds;
	milliSecond = newMilli;
	lastSync = currentTime;

}

void Time_lock(){
	currentTimeLock = currentTime;
	milliSecondLock = milliSecond;
}


/* [] END OF FILE */
