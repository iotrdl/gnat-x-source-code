/*
Copyright 2014 Deep Information Sciences, Inc. and the University of New Hampshire (UNH)
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
*
http://www.apache.org/licenses/LICENSE-2.0
*
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*
*
@author Thomas P Kimsey
8/20/13
*
*/

static uint32 currentTime;
static uint16 milliSecond;
static uint32 currentTimeLock;
static uint16 milliSecondLock;
static uint32 lastSync;
static uint32 previousTime;
static uint16 previousMillis;

static uint8 updated;
void Time_Start(uint8 mode);
void Time_sync();
uint32 Time_getTime();
uint16 Time_getMilliseconds();
uint8 Time_getTime0();
uint8 Time_getTime1();
uint8 Time_getTime2();
uint8 Time_getTime3();
uint8 Time_getTime4();
uint8 Time_getTime5();
void Time_addSecond();
void Time_addMilliSecond();
void Time_setTime(uint32 t0, uint32 t1, uint32 t2, uint32 t3, uint32 t4, uint32 t5);
uint8 Time_isUpdated();
void Time_lock();

//[] END OF FILE
