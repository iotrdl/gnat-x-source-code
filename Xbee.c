/*
Copyright 2014 Deep Information Sciences, Inc. and the University of New Hampshire (UNH)
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
*
http://www.apache.org/licenses/LICENSE-2.0
*
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*
*
@author Thomas P Kimsey
8/20/13
*
*/
#include <device.h>
#include <Xbee.h>

void Xbee_Start(){
	XbeeUart_Start();
}

uint8 Xbee_Read(){
	uint8 byte = XbeeUart_ReadRxData();
	if (byte == 0xFE &&  Xbee_GetRxBufferSize() > 0)  return XbeeUart_ReadRxData();
	return byte;
}

void Xbee_Write(uint8 dout){
	if ( dout < 0xFE){
		XbeeUart_WriteTxData(dout);
		CyDelay(1);
	}
	else if( dout == 0xFF){
		XbeeUart_WriteTxData(0xFE);
		CyDelay(1);
		XbeeUart_WriteTxData(0xFF);
		CyDelay(1);
		}
	else{
		XbeeUart_WriteTxData(0xFE);
		CyDelay(1);
		XbeeUart_WriteTxData(0xFE);
		CyDelay(1);
	}
}

void Xbee_WriteSpecial(){
	XbeeUart_WriteTxData(0xFF);
}



uint8 Xbee_GetRxBufferSize(){
	return XbeeUart_GetRxBufferSize();
}

void Xbee_ClearRxBuffer(){
	XbeeUart_ClearRxBuffer();
}

/* [] END OF FILE */
