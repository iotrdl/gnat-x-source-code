/*
Copyright 2014 Deep Information Sciences, Inc. and the University of New Hampshire (UNH)
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
*
http://www.apache.org/licenses/LICENSE-2.0
*
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*
*
@author Thomas P Kimsey
8/20/13
*
*/


#include <device.h>
#include <W5100.h>
#include <W5100_1.h>
#include <Time.h>
#include <Xbee.h>
#include <Packet.h>
#include <mqtt.h>
#include <JSON.h>
#include <stdio.h>
extern uint8 JSONPacket[255];

/* ----------------------------- 
* Device Mode
* 0 = No Network Function
* 1 = Ethernet
* 2 = Wireless
* 3 = Base Station
*/


uint8 mode; 




uint8 value;

void main()
{	
	uint16 * const ptr;
	uint16 j;
	uint8 i =0;
	uint8 total;
	uint8 packetq[255];
	uint8 packet[10];
	uint16 degrees = 0;
	uint32 timeTemp;
	cystatus Status;
	
	//----------------------------------------------------------------------------
	// IOT Node Initialization Code
	//
	// All the code in this section should not be modified other than the 
	// function parameters
	// Please read the Hardware User Guide for help
	//----------------------------------------------------------------------------
	
	CyGlobalIntEnable;
	
	mode = 1;                                                  // <------ MUST BE SET!

	
	if ( mode == 1 ||  mode == 3 ){
		W5100_Start();
		W5100_setDeviceIp( 132, 177, 206, 25);                    // <------ MUST BE SET!
		W5100_setGateAddr(  132, 177, 206, 1);                    // <------ MUST BE SET!
		W5100_setMacAddr(0x00, 0x08, 0xDC, 0x00, 0x00, 0x00);     // <------ MUST BE SET!
		W5100_setSubAddr(255, 255, 255, 0 );                      // <------ MUST BE SET!
		W5100_setSourcePort(0x07, 0x5b );                         // <------ MUST BE SET!
		W5100_setDestPort( 0x07, 0x5b );                          // <------ MUST BE SET!
		W5100_setDestIp( 166,78,112,162 );                    // <------ MUST BE SET!
		Time_Start(mode);
	}
	
	if (mode == 2 || mode == 3 ){
		Xbee_Start();	
		Time_Start(mode);
	}
	
	
	//Starts a tcp conversation with server
	W5100_TCP_Connect();
	CyDelay(300);


	
	//connect to mqtt server with a 130 second timeout
	mqtt_connect(130, "testDevice");
	
	//waits untill server has acknowledged connection
	while(mqtt_connected() == 0x00){
		mqtt_loop();
	}
	
	mqtt_setUUID("00ad9f90-da9f-11e2-a28f-0800200c9a66");
	
	/*Test for subscribe topics */
	//mqtt_subscribe(5, "temp", 0);
	
	
	//Starts analog to digital converter
	//In this case taking temperature measurement
	ADC_Start();
	ADC_StartConvert();
	CyDelay(10); //Let the adc have time to get a sample. 
	
	
	
	
	
	//Main loop
	while (1 == 1){
		i++;
		
		
		mqtt_loop();  //reads and processes any recieved mqtt messages 
		
		
		//---------------------------------------------------
		//Test code 
		//Makes sure the connection still exists 
		//before each publish
		mqtt_pingreq();
		while(mqtt_connected() == 0x00){
			mqtt_loop();
		}
		//End Test Code
		//---------------------------------------------------
		
		
		//Reads the temp sensor and sends the degrees in 
		// C to topic "temp"
		j = ADC_GetResult16();
		degrees =(ADC_CountsTo_mVolts(j)-500); //conversion for tmp 36 sensor
		sprintf(packet,"%d",degrees);
		JSON_Clear();
		JSON_addString("{\"Sensor\":\"Temperature\", \"Degrees C\":");
		JSON_addChar(packet[0]);
		JSON_addChar(packet[1]);
		JSON_addString(".");
		JSON_addChar(packet[2]);
		JSON_addString(", \"Time\": ");
		Time_lock();
		timeTemp = Time_getTime();
		sprintf(packet,"%lu",timeTemp);
		JSON_addChar(packet[0]);
		JSON_addChar(packet[1]);
		JSON_addChar(packet[2]);
		JSON_addChar(packet[3]);
		JSON_addChar(packet[4]);
		JSON_addChar(packet[5]);
		JSON_addChar(packet[6]);
		JSON_addChar(packet[7]);
		JSON_addChar(packet[8]);
		JSON_addChar(packet[9]);
		
		JSON_addString("}");

		mqtt_publish("Node/00ad9f90-da9f-11e2-a28f-0800200c9a66/Sensor/S226Temp", JSONPacket, JSON_getIndex());
		
		
		
		if( i % 32 ==0){
			Status = DieTemp_GetTemp(&j);
			sprintf(packet,"%d",Status);
			JSON_Clear();
			JSON_addString("{\"Sensor\":\"Temperature\", \"Degrees C\":");
			JSON_addChar(packet[0]);
			JSON_addChar(packet[1]);
			JSON_addString(".");
			JSON_addChar(packet[2]);
			JSON_addString(", \"Time\": ");
			Time_lock();
			timeTemp = Time_getTime();
			sprintf(packet,"%lu",timeTemp);
			JSON_addChar(packet[0]);
			JSON_addChar(packet[1]);
			JSON_addChar(packet[2]);
			JSON_addChar(packet[3]);
			JSON_addChar(packet[4]);
			JSON_addChar(packet[5]);
			JSON_addChar(packet[6]);
			JSON_addChar(packet[7]);
			JSON_addChar(packet[8]);
			JSON_addChar(packet[9]);
			JSON_addString("}");
			mqtt_publish("Node/00ad9f90-da9f-11e2-a28f-0800200c9a66/Sensor/DieTemp", JSONPacket, JSON_getIndex());
		}

		CyDelay(10000); //1 second

		
		
	}
	
	//Never reached but would end the tcp coversation
	W5100_TCP_Disconnect();
	
	
}	
//[] END OF FILE
