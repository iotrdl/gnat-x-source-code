/*
Copyright 2014 Deep Information Sciences, Inc. and the University of New Hampshire (UNH)
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
*
http://www.apache.org/licenses/LICENSE-2.0
*
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*
*
@author Thomas P Kimsey
8/20/13
*
*/
#include <device.h>
#include <W5100.h>

extern uint8 dataRec[255];
uint8 uuid[36];
uint8 d[255];
uint8 qos = 0;
uint8 connected = 0;
uint32 lastHello = 0;

// Connects to a mqtt server
// ip of server and proper port settings 
// must be configured in w5100.c
// timeout = timeout in seconds
// id = device id in string form
void mqtt_connect(uint8 timeout, char8 * id){
	
	uint8 length = 16;
	uint8 indexU8 = 1u;
    char8 current = *id;
	
	
	//Fixed Header
	d[0]= 0x10;
	// add d[1] = length
	
	
	
	//variable header
	//protocol name
	d[2] = 0;
	d[3] = 6;
	d[4] = 0x4d; //M
	d[5] = 0x51; //Q
	d[6] = 0x49; //I
	d[7] = 0x73; //s
	d[8] = 0x64; //d
	d[9] = 0x70; //p
	
	//protocol version number
	d[10] = 3; // version 3
	
	//connect flags
	d[11] = 0x02;
	
	//keep alive timer
	d[12] = 0;
	d[13] = timeout;
	
	
	//device id
	while(current != (char8) '\0')
    {
        d[length] = (current);
        current = *(id + indexU8);
        indexU8++;
		length ++;
    }
	
	d[14] = 0;
	d[15] = length - 16;
	
	
	d[1] = length -2;
	W5100_send(d, length);
	
}


void mqtt_setUUID(char8 * id){
	uint8 indexU8 = 1u;
    char8 current = *id;
	uint8 length = 0;
	while(current != (char8) '\0')
	{
	    uuid[length] = (current);
	    current = *(id + indexU8);
	    indexU8++;
		length++;

	}
}

// returns if the device is connected to 
// a mqtt server
// returns 0x01 if connected
// 0x00 if not
uint8 mqtt_connected(){
	return connected;
}


// mqtt publish message
// subject = topic being published to in string form
// dout = payload of publish message
// size = size of dout
void mqtt_publish( char8 * subject, uint8 dout[], uint8 size){
	uint8 i;
	uint8 length = 4;
	uint8 indexU8 = 1u;
    char8 current = *subject;
	
	//Fixed Header
	d[0]= 0x30;
	
	
	
	//variable header
	
	while(current != (char8) '\0')
	{
	    d[length] = (current);
	    current = *(subject + indexU8);
	    indexU8++;
		length ++;
	}
	
	d[2] = 0;
	d[3] = length-4;
	d[1] = length + size-2;
	
	for ( i = 0; i < size; i++){
		
		d[length+i] = dout[i];
	}
	
	W5100_send(d, length +size );

}


// Subscribes to a specific topic 
// Not fully tested

// each subscribe message must have a unique id because
// subscribe has a qos of 1
void mqtt_subscribe(uint8 id, char8 * topic, uint8 qos){
	
	uint8 length = 0;
	uint8 indexU8 = 1u;
    char8 current = *topic;
	
	//Fixed Header
	d[0]= 0x80;
	
	d[2] = 0x00;
	d[3] = id;
	
	while(current != (char8) '\0')
    {
        d[length+6] = (current);
        current = *(topic + indexU8);
        indexU8++;
		length ++;
    }
	d[length+6] = qos;
	d[4] = 0;
	d[5] = length;
	d[1] = length + 5;
	W5100_send(d, length + 7);
}

// Requests a ping response from the server
void mqtt_pingreq(){
	
	//Set connected flag to 0 (disconnected)
	//Once a ping response is recieved it will
	//be set back to 1 (connected)
	connected = 0;
	
	//Fixed Header
	d[0]= 0xC0;
	d[1] = 0;
	W5100_send(d, 2 );

}

// Responds to a ping request from the server
void mqtt_pingresp(){
	
	//Fixed Header
	d[0]= 0xD0;
	d[1] = 0;
	W5100_send(d, 2 );
}


//Disconnects the device from an mqtt server
void mqtt_disconnect(){
	
	//Sets connected to 0
	connected = 0x00;

	//Fixed Header
	d[0]= 0xE0;
	d[1] = 0;
	W5100_send(d, 2 );

}

//Control loop to read and process inbound messages
//This function should be called often 
void mqtt_loop(){

	//Wired
	if ( /*(modeOp == 1 || modeOp == 3 )&&*/ W5100_dataReceived() == 0x01 ){
		W5100_readTCP();
		
		//Connack
		if( dataRec[0] == 0x20){
			connected = 0x01;
		}
		
		//pingreq
		else if (  dataRec[0] == 0xC0){
			mqtt_pingresp();
			
		}
		
		//pingresp
		else if (  dataRec[0] == 0xD0){
			connected = 0x01;
		}
		
		//publish message
		else if (dataRec[0] == 0x30){
			CyDelay(1); //dummy insert for debugging
		}
		
		
	}	

}


/* [] END OF FILE */
