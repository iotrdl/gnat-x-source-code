/*
Copyright 2014 Deep Information Sciences, Inc. and the University of New Hampshire (UNH)
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
*
http://www.apache.org/licenses/LICENSE-2.0
*
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*
*
@author Thomas P Kimsey
8/20/13
*
*/
#include "cytypes.h"
#include "cyfitter.h"

//void mqtt_connect(void);
void mqtt_connect(uint8 timeout, char8 *id);
void mqtt_setUUID(char8 * id);
uint8 mqtt_connected(void);
void mqtt_publish(char8 * subject, uint8 dout[], uint8 size);
void mqtt_subscribe(uint8 id, char8 * topic, uint8 qos);
void mqtt_pingreq(void);
void mqtt_pingresp(void);
void mqtt_disconnect(void);
void mqtt_loop(void);
//[] END OF FILE
