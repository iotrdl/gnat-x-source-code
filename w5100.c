/*
Copyright 2014 Deep Information Sciences, Inc. and the University of New Hampshire (UNH)
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
*
http://www.apache.org/licenses/LICENSE-2.0
*
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*
*
@author Thomas P Kimsey
8/20/13
*
*/

#include <device.h>
#include <w5100.h>


uint8 dataRec[255];

void W5100_Start(){

	uint16 j = 0;
	for( j = 0; j < 255; j++){
		packet[j] = 0;
	}
	// Starts the SPI Component
	// This allows for SPI communication 
	// With the W5100
	SPI_Start();
	
	
	//Reset the W5100
	W5100_spiSend8(0xF0,0x00,0x00,0x80);
	
	
	// Sets the receive memory size 
	// 2Kb
	W5100_spiSend8(0xF0,0x00, 0x1A, 0x55);
	
	// Sets the transmit memory size 
	// 2Kb
	W5100_spiSend8(0xF0,0x00, 0x1B, 0x55);
	
	gS0_TX_BASE = TXBUF_BASE;
	gS0_RX_BASE = RXBUF_BASE;
	gS0_TX_MASK = 0x07FF;
	gS0_RX_MASK = 0x07FF;
	
	protocol = 0x01;
	
	if(protocol == 0x00){
	
		W5100_setUDP();
	}
	else if (protocol == 0x01){
		W5100_Set_TCP();
	}
	

	
}

void W5100_setUDP(){
	
	
	/* --------------------------------------------
	*  Procedure to set the W5100 in UDP mode
	*  Socket 0
	* -------------------------------------------- */
	
	// Sets UDP Mode
	W5100_spiSend16( 0xF0, S0_MR, 0x02 );
	
	// Set up for communication on UDP port 26
	W5100_spiSend16( 0xF0, S0_PORT_0, 00);
	W5100_spiSend16( 0xF0, S0_PORT_1, 26);
	
	// Sets the Port to OPEN
	W5100_spiSend16( 0xF0, S0_CR, 0x01);

	// Check to see if W5100 was successfully 
	// Put into UDP mode

	S0_SR_VALUE = W5100_spiRead8( S0_SR );
	
	if ( S0_SR_VALUE != 0x22 ){
		W5100_spiSend16(0xF0, S0_CR, 0x10);
		//CR_Write(0x02);
		while(1){
		}
	}
	else{
		//CR_Write(0x01);
	}

}

void W5100_Set_TCP(){
	
	
	/* --------------------------------------------
	*  Procedure to set the W5100 in TCP mode
	*  Socket 0
	* -------------------------------------------- */
	
	// Sets TCP Mode
	W5100_spiSend16( 0xF0, S0_MR, 0x01 );
	
		// Sets the Port to OPEN
	W5100_spiSend16( 0xF0, S0_CR, 0x01);
	
	

}

void W5100_TCP_Connect(){
	// Sets the Port to CONNECT
	W5100_spiSend16( 0xF0, S0_CR, 0x04);
}

uint8 W5100_TCP_Established(){
	S0_SR_VALUE = W5100_spiRead8( S0_SR );
	if(S0_SR_VALUE == 0x17 ){
		return 0x01;
	}
	else{
		return S0_SR_VALUE;
	}
}
	
void W5100_TCP_Disconnect(){
		// Sets the Port to disconnected
	W5100_spiSend16( 0xF0, S0_CR, 0x08);
}


void W5100_Socket_Close(){
	// Sets the Port to Closed
	W5100_spiSend16( 0xF0, S0_CR, 0x10);
}


// Sends a Byte of data to addr1 << 8 + addr2
// via spi communication with w5100
// ******** Outdated and needs to be changed *************
void W5100_spiSend8( uint8 rW, uint8 addr1, uint8 addr2, uint8 din){
	SPI_WriteTxData(rW);
	SPI_WriteTxData(addr1);
	SPI_WriteTxData(addr2);
	SPI_WriteTxData(din);
	CyDelayUs(20);
}


// Sends a Byte of data to addr 
// via spi communication with w5100
void W5100_spiSend16( uint8 rW, uint16 addr, uint8 din){
	uint8 addr1 = addr >> 8;
	uint8 addr2 = addr & 0xFF;
	SPI_WriteTxData(rW);
	SPI_WriteTxData(addr1);
	SPI_WriteTxData(addr2);
	SPI_WriteTxData(din);
	CyDelayUs(20);
}


// Reads a Byte of data stored at addr
// via spi communication with w5100
uint8 W5100_spiRead8( uint16 addr ){
	uint8 addr1 = addr >> 8;
	uint8 addr2 = addr & 0xFF;
	SPI_ClearRxBuffer();
	SPI_WriteTxData(0x0F);
	SPI_WriteTxData(addr1);
	SPI_WriteTxData(addr2);
	SPI_WriteTxData(0x00);
	CyDelayUs(25);
	if( SPI_GetRxBufferSize() > 0){
		SPI_ReadRxData();
		SPI_ReadRxData();
		SPI_ReadRxData();
		return SPI_ReadRxData();
	}
	return 0xFF;
}

// Reads a 16 bit data value from addr
// via spi communication with w5100
// *********  Could fail from incorrect address calculation ********
uint16 W5100_spiRead16( uint16 addr ){
	uint16 upper;
	uint8 lower;
	uint8 addr1 = addr >> 8;
	uint8 addr2 = addr & 0xFF;
	SPI_ClearRxBuffer();
	
	// First Byte
	SPI_WriteTxData(0x0F);
	SPI_WriteTxData(addr1);
	SPI_WriteTxData(addr2);
	SPI_WriteTxData(0x00);
	CyDelayUs(25);
	if( SPI_GetRxBufferSize() > 0){
		SPI_ReadRxData();
		SPI_ReadRxData();
		SPI_ReadRxData();
		upper = SPI_ReadRxData();
	}
	
	// Second Byte
	SPI_WriteTxData(0x0F);
	SPI_WriteTxData(addr1);
	SPI_WriteTxData(addr2+1);
	SPI_WriteTxData(0x00);
	CyDelayUs(20);
	if( SPI_GetRxBufferSize() > 0){
		SPI_ReadRxData();
		SPI_ReadRxData();
		SPI_ReadRxData();
		lower = SPI_ReadRxData();
	}
	
	// Returns the combination of the 2 bytes
	// as a 16 bit unsigned integer 
	return (upper << 8 ) + lower;
}

//returns 0x01 if data has been recieved
// 0x00 if not
uint8 W5100_dataReceived(){
	uint16 temp;
	temp =  W5100_spiRead8(S0_RX_RSR0);
	temp = temp << 8;
	temp += W5100_spiRead8(S0_RX_RSR1);
	if( temp > 0 ) return 1;
	return 0;
	//temp = spiRead8(S0_IR);
	//return (spiRead8(S0_IR) & 0x04 );
	
}

// Reads in recieved UDP data
void W5100_readUDP(){
	
	uint16 temp;
	uint16 header_size = 8;
	uint16 upper_size;
	uint16 i;
	uint8 RX_RD_0;
	uint8 RX_RD_1;

	
	temp =  W5100_spiRead8(S0_RX_RSR0);
	get_size = temp << 8;
	get_size += W5100_spiRead8(S0_RX_RSR1);
	
	// calculates the starting address to 
	// read packet data from
	
	temp =  W5100_spiRead8(S0_RX_RD0);
	RX_RD = temp << 8;
	RX_RD += W5100_spiRead8(S0_RX_RD1);
	
	get_offset = RX_RD & gS0_RX_MASK;
	get_start_address = gS0_RX_BASE + get_offset;
	
	
	// Read the UDP Header data
	if (( get_offset + header_size ) > ( gS0_RX_MASK + 1 )){
		
		upper_size = (gS0_RX_MASK + 1) - get_offset;
		
		for( i = 0; i < upper_size; i++){
			headerRec[i] = W5100_spiRead8( get_start_address + i );
		}
		
		for( i = upper_size; i < header_size; i++){
			 headerRec[i] = W5100_spiRead8(gS0_RX_BASE + i - upper_size );
		}
		
		get_offset = header_size - upper_size;
	}
	
	else {
		
		for( i = 0; i < header_size; i++){
			headerRec[i] = W5100_spiRead8( get_start_address + i );
		}
		
		get_offset += header_size;
	}
	
	get_start_address = gS0_RX_BASE + get_offset;
	
	
	// Sets the peer ip address to the 
	// one received from the UDP packet
	peer_ip[0] = headerRec[0];
	peer_ip[1] = headerRec[1];
	peer_ip[2] = headerRec[2];
	peer_ip[3] = headerRec[3];
	
	// saves the port being used
	temp = headerRec[4];
	peer_port = temp << 8 + headerRec[5];
	
	// Saves the amount of data held in
	// the packet
	temp = headerRec[6];
	get_size = (temp << 8) + headerRec[7];
	
	
	// Read in the UDP Packet data
	if (( get_offset + get_size ) > ( gS0_RX_MASK + 1 )){
		
		upper_size = (gS0_RX_MASK + 1) - get_offset;
		
		for( i = 0; i < upper_size; i++){
			dataRec[i] = W5100_spiRead8( get_start_address + i );
		}
		
		for( i = upper_size; i < get_size; i++){
			 dataRec[i] = W5100_spiRead8(gS0_RX_BASE + i - upper_size );
		}
		
	}
	
	else {
		
		for( i = 0; i < get_size; i++){
			dataRec[i] = W5100_spiRead8( get_start_address + i );
		}
		
	}

	// Set the read pointer to indicated
	// that the new data has been read
	RX_RD += get_size + header_size;
	RX_RD_0 = RX_RD >> 8;
	RX_RD_1 = RX_RD & 0xFF;
	
	W5100_spiSend16( 0xF0, S0_RX_RD0, RX_RD_0);
	W5100_spiSend16( 0xF0, S0_RX_RD1, RX_RD_1);
	
	// send the received command
	// to Socket 0 Control Register
	W5100_spiSend16( 0xF0, S0_CR, 0x40);

}

void W5100_readTCP(){


	uint16 temp;
	uint16 header_size = 20;
	uint16 upper_size;
	uint16 i;
	uint8 RX_RD_0;
	uint8 RX_RD_1;

	
	temp =  W5100_spiRead8(S0_RX_RSR0);
	get_size = temp << 8;
	get_size += W5100_spiRead8(S0_RX_RSR1);
	
	// calculates the starting address to 
	// read packet data from
	
	temp =  W5100_spiRead8(S0_RX_RD0);
	RX_RD = temp << 8;
	RX_RD += W5100_spiRead8(S0_RX_RD1);
	
	get_offset = RX_RD & gS0_RX_MASK;
	get_start_address = gS0_RX_BASE + get_offset;
	
	
	
	// Read in the TCP Packet data
	if (( get_offset + get_size ) > ( gS0_RX_MASK + 1 )){
		
		upper_size = (gS0_RX_MASK + 1) - get_offset;
		
		for( i = 0; i < upper_size; i++){
			dataRec[i] = W5100_spiRead8( get_start_address + i );
		}
		
		for( i = upper_size; i < get_size; i++){
			 dataRec[i] = W5100_spiRead8(gS0_RX_BASE + i - upper_size );
		}
		
	}
	
	else {
		
		for( i = 0; i < get_size; i++){
			dataRec[i] = W5100_spiRead8( get_start_address + i );
		}
		
	}

	// Set the read pointer to indicated
	// that the new data has been read
	RX_RD += get_size;
	RX_RD_0 = RX_RD >> 8;
	RX_RD_1 = RX_RD & 0xFF;
	
	W5100_spiSend16( 0xF0, S0_RX_RD0, RX_RD_0);
	W5100_spiSend16( 0xF0, S0_RX_RD1, RX_RD_1);
	
	// send the received command
	// to Socket 0 Control Register
	W5100_spiSend16( 0xF0, S0_CR, 0x40);
	
}


// Sends a packet of data to the 
// destination ip address via UDP
void W5100_send( uint8 info[], uint8 length){

	uint16 temp;
	uint8 TX_WR_0;
	uint8 TX_WR_1;
	uint16 upper_size;
	int i;
	
	// waits for space to become availible in TX
	// memory
	uint16 get_free_size =  W5100_spiRead16(S0_TX_FSR0);
	while ( get_free_size < 4 ){
		get_free_size =  W5100_spiRead16(S0_TX_FSR0);
	}


	
	// Reads the TX write pointer
	// from w5100 memory
	temp =  W5100_spiRead8(0x0424);
	TX_WR = temp << 8;
	TX_WR += W5100_spiRead8(0x0425);
	
	// calculates the starting address to 
	// write packet data to
	get_offset = TX_WR & gS0_TX_MASK;
	get_start_address = gS0_TX_BASE + get_offset;
	
	// check for end of TX memory and loops back to 
	// start address if needed
	if ( (get_offset + length) > (gS0_TX_MASK + 1) )
	{
		//CR_Write(0x03);
		upper_size = (gS0_TX_MASK + 1 ) - get_offset ;
		for( i = 0; i < upper_size; i++){
			 W5100_spiSend16( 0xF0, get_start_address + i, info[i] );
		}
		for( i = upper_size; i < length; i++){
			 W5100_spiSend16( 0xF0, gS0_TX_BASE+ i - upper_size, info[i] );
		}
	}
	
	// if memory space will not run out send data normally
	else
	{
		for( i = 0; i < length; i++){
			 W5100_spiSend16( 0xF0, get_start_address + i, info[i] );
		}
	}
	
	/* increase Sn_TX_WR as length of send_size */
	TX_WR += length;
	TX_WR_0 = TX_WR >> 8;
	TX_WR_1 = TX_WR & 0xFF;
	
	W5100_spiSend16( 0xF0, 0x0424, TX_WR_0);
	W5100_spiSend16( 0xF0, 0x0425, TX_WR_1);

	/* set SEND command */
	W5100_spiSend16( 0xF0, S0_CR, 0x20);
	
	// waits for socket 0 to acknowledge 
	// the send command
	//while ( W5100_spiRead8( S0_CR) != 0x00 );
}

//reads the first two digits of the port number
uint8 W5100_readPort0(){
	return W5100_spiRead8(S0_DPORT_0);
}

//reads the second two digits of the port number
uint8 W5100_readPort1(){
	return W5100_spiRead8(S0_DPORT_1);
}

//sets the port of the w5100
void W5100_setDestPort(uint8 p1, uint8 p2){
	W5100_spiSend16( 0xF0, S0_DPORT_0, p1);  
	W5100_spiSend16( 0xF0, S0_DPORT_1, p2); 
}

//Sets the port number for sending
void W5100_setSourcePort(uint8 p1, uint8 p2){
	W5100_spiSend16( 0xF0, S0_PORT_0, p1);  
	W5100_spiSend16( 0xF0, S0_PORT_1, p2); 
}

//methods to read the 4 parts of the
//destination ip address
uint8 W5100_readIp0(){
	return W5100_spiRead8(S0_DIPR_0);
}
uint8 W5100_readIp1(){
	return W5100_spiRead8(S0_DIPR_1);
}
uint8 W5100_readIp2(){
	return W5100_spiRead8(S0_DIPR_2);
}
uint8 W5100_readIp3(){
	return W5100_spiRead8(S0_DIPR_3);
}


// Sets the destination ip address
// format: ip1.ip2.ip3.ip4
void W5100_setDestIp(uint8 ip1, uint8 ip2, uint8 ip3, uint8 ip4){
	W5100_spiSend16( 0xF0, S0_DIPR_0, ip1 ); 
	W5100_spiSend16( 0xF0, S0_DIPR_1, ip2 );  
	W5100_spiSend16( 0xF0, S0_DIPR_2, ip3 );  
	W5100_spiSend16( 0xF0, S0_DIPR_3, ip4 ); 
}

void W5100_setDeviceIp(uint8 ip1, uint8 ip2, uint8 ip3, uint8 ip4){
	// Sets the ip address of the device
	W5100_spiSend16( 0xF0, SIPR0, ip1);       // 132
	W5100_spiSend16( 0xF0, SIPR1, ip2);       // 177
	W5100_spiSend16( 0xF0, SIPR2, ip3);       // 206
	W5100_spiSend16( 0xF0, SIPR3, ip4);       // 25
}

void W5100_setGateAddr(uint8 addr0, uint8 addr1, uint8 addr2, uint8 addr3){
	// Sets the network gateway address
	W5100_spiSend16( 0xF0, GAR0, addr0);        
	W5100_spiSend16( 0xF0, GAR1, addr1);        
	W5100_spiSend16( 0xF0, GAR2, addr2);        
	W5100_spiSend16( 0xF0, GAR3, addr3);        
}


void W5100_setSubAddr(uint8 addr0, uint8 addr1, uint8 addr2, uint8 addr3){
	// Sets the network subnet address
	W5100_spiSend16( 0xF0, SUBR0, addr0);       
	W5100_spiSend16( 0xF0, SUBR1, addr1);       
	W5100_spiSend16( 0xF0, SUBR2, addr2);       
	W5100_spiSend16( 0xF0, SUBR3, addr3);       
}

void W5100_setMacAddr(uint8 mac0, uint8 mac1, uint8 mac2, uint8 mac3, uint8 mac4, uint8 mac5){
	
	// Sets the mac address of the device
	W5100_spiSend16( 0xF0, SHAR0, mac0);
	W5100_spiSend16( 0xF0, SHAR1, mac1);
	W5100_spiSend16( 0xF0, SHAR2, mac2);
	W5100_spiSend16( 0xF0, SHAR3, mac3);
	W5100_spiSend16( 0xF0, SHAR4, mac4);
	W5100_spiSend16( 0xF0, SHAR5, mac5);
}

/* [] END OF FILE */
