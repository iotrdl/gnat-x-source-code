/*
Copyright 2014 Deep Information Sciences, Inc. and the University of New Hampshire (UNH)
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
*
http://www.apache.org/licenses/LICENSE-2.0
*
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*
*
@author Thomas P Kimsey
8/20/13
*
*/
// memory setup values
static const uint16 S_SIZE = 2048;


static const uint16 TXBUF_BASE = 0x4000;
static const uint16 RXBUF_BASE = 0x6000;
static uint16 gS0_TX_BASE;
static uint16 gS0_RX_BASE;
static uint16 gS0_TX_MASK;
static uint16 gS0_RX_MASK;

// Common Registers
// See the W5100 Datasheet for register descriptions
static const uint16 MR    = 0x0000;
static const uint16 GAR0  = 0x0001;
static const uint16 GAR1  = 0x0002;
static const uint16 GAR2  = 0x0003;
static const uint16 GAR3  = 0x0004;
static const uint16 SUBR0 = 0x0005;
static const uint16 SUBR1 = 0x0006;
static const uint16 SUBR2 = 0x0007;
static const uint16 SUBR3 = 0x0008;
static const uint16 SHAR0 = 0x0009;
static const uint16 SHAR1 = 0x000A;
static const uint16 SHAR2 = 0x000B;
static const uint16 SHAR3 = 0x000C;
static const uint16 SHAR4 = 0x000D;
static const uint16 SHAR5 = 0x000E;
static const uint16 SIPR0 = 0x000F;
static const uint16 SIPR1 = 0x0010;
static const uint16 SIPR2 = 0x0011;
static const uint16 SIPR3 = 0x0012;
static const uint16 RMSR  = 0x001A;
static const uint16 TMSR  = 0x001B;

// Socket 0 Register Addresses
// See the W5100 Datasheet for register descriptions
static const uint16 S0_MR      = 0x0400;
static const uint16 S0_CR      = 0x0401;
static const uint16 S0_IR      = 0x0402;
static const uint16 S0_SR      = 0x0403;
static const uint16 S0_PORT_0  = 0x0404;
static const uint16 S0_PORT_1  = 0x0405;
static const uint16 S0_DHAR0   = 0x0406;
static const uint16 S0_DHAR1   = 0x0407;
static const uint16 S0_DHAR2   = 0x0408;
static const uint16 S0_DHAR3   = 0x0409;
static const uint16 S0_DHAR4   = 0x040A;
static const uint16 S0_DHAR5   = 0x040B;
static const uint16 S0_DIPR_0  = 0x040C;
static const uint16 S0_DIPR_1  = 0x040D;
static const uint16 S0_DIPR_2  = 0x040E;
static const uint16 S0_DIPR_3  = 0x040F;
static const uint16 S0_DPORT_0 = 0x0410;
static const uint16 S0_DPORT_1 = 0x0411;
static const uint16 S0_TX_FSR0 = 0x0420;
static const uint16 S0_TX_FSR1 = 0x0421;
static const uint16 S0_TX_WR_0 = 0x0424;
static const uint16 S0_TX_WR_1 = 0x0425;
static const uint16 S0_RX_RSR0 = 0x0426;
static const uint16 S0_RX_RSR1 = 0x0427;
static const uint16 S0_RX_RD0  = 0x0428;
static const uint16 S0_RX_RD1  = 0x0429;


static uint8 S0_SR_VALUE;

static uint8 protocol;


// Received UDP packet data
static uint8 headerRec[8];

static uint8 peer_ip[4];
static uint16 peer_port;
static uint16 get_size;

// Variables for reading and
// writing to TX and RX memory
static uint16 TX_WR = 0;
static uint16 RX_RD = 0;
static uint16 get_offset;
static uint16 get_start_address;

static uint8 packet[255];

void W5100_Start(void);
void W5100_setUDP(void);
void W5100_Set_TCP(void);
void W5100_TCP_Connect(void);
uint8 W5100_TCP_Established(void);
void W5100_TCP_Disconnect(void);
void W5100_Socket_Close(void);
void W5100_spiSend8( uint8, uint8, uint8, uint8);
void W5100_send(uint8 info[], uint8 length);
void W5100_readTCP(void);
void W5100_readUDP(void);
uint8 W5100_dataReceived();
uint16 W5100_spiRead16( uint16);
uint8 W5100_spiRead8( uint16);
void W5100_spiSend16( uint8, uint16, uint8);
uint8 W5100_readPort0();
uint8 W5100_readPort1();
void W5100_setSourcePort(uint8, uint8);
void W5100_setDestPort(uint8, uint8);
uint8 W5100_readIp0();
uint8 W5100_readIp1();
uint8 W5100_readIp2();
uint8 W5100_readIp3();
void W5100_setDestIp(uint8 ip1, uint8 ip2, uint8 ip3, uint8 ip4);
void W5100_setDeviceIp(uint8 ip1, uint8 ip2, uint8 ip3, uint8 ip4);
void W5100_setGateAddr(uint8 addr0, uint8 addr1, uint8 addr2, uint8 addr3);
void W5100_setSubAddr(uint8 addr0, uint8 addr1, uint8 addr2, uint8 addr3);
void W5100_setMacAddr(uint8 mac0, uint8 mac1, uint8 mac2, uint8 mac3, uint8 mac4, uint8 mac5);
	

//[] END OF FILE
