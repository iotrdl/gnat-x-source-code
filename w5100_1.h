/*
Copyright 2014 Deep Information Sciences, Inc. and the University of New Hampshire (UNH)
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
*
http://www.apache.org/licenses/LICENSE-2.0
*
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*
*
@author Thomas P Kimsey
8/20/13
*
*/
static const uint16 gS1_TXBUF_BASE = 0x4800;
static const uint16 gS1_RXBUF_BASE = 0x6800;
static uint16 gS1_TX_BASE;
static uint16 gS1_RX_BASE;
static uint16 gS1_TX_MASK;
static uint16 gS1_RX_MASK;

// Socket 0 Register Addresses
// See the W5100 Datasheet for register descriptions
static const uint16 S1_MR      = 0x0500;
static const uint16 S1_CR      = 0x0501;
static const uint16 S1_IR      = 0x0502;
static const uint16 S1_SR      = 0x0503;
static const uint16 S1_PORT_0  = 0x0504;
static const uint16 S1_PORT_1  = 0x0505;
static const uint16 S1_DHAR0   = 0x0506;
static const uint16 S1_DHAR1   = 0x0507;
static const uint16 S1_DHAR2   = 0x0508;
static const uint16 S1_DHAR3   = 0x0509;
static const uint16 S1_DHAR4   = 0x050A;
static const uint16 S1_DHAR5   = 0x050B;
static const uint16 S1_DIPR_0  = 0x050C;
static const uint16 S1_DIPR_1  = 0x050D;
static const uint16 S1_DIPR_2  = 0x050E;
static const uint16 S1_DIPR_3  = 0x050F;
static const uint16 S1_DPORT_0 = 0x0510;
static const uint16 S1_DPORT_1 = 0x0511;
static const uint16 S1_TX_FSR0 = 0x0520;
static const uint16 S1_TX_FSR1 = 0x0521;
static const uint16 S1_TX_WR_0 = 0x0524;
static const uint16 S1_TX_WR_1 = 0x0525;
static const uint16 S1_RX_RSR0 = 0x0526;
static const uint16 S1_RX_RSR1 = 0x0527;
static const uint16 S1_RX_RD0  = 0x0528;
static const uint16 S1_RX_RD1  = 0x0529;


static uint8 S1_SR_VALUE;

// Received UDP packet data
static uint8 S1_headerRec[8];

static uint8 S1_peer_ip[4];
static uint16 S1_peer_port;
static uint16 S1_get_size;

// Variables for reading and
// writing to TX and RX memory
static uint16 S1_TX_WR = 0;
static uint16 S1_RX_RD = 0;
static uint16 S1_get_offset;
static uint16 S1_get_start_address;

static uint8 S1_packet[255];

void W5100_1_Start(void);
void W5100_1_spiSend8( uint8, uint8, uint8, uint8);
void W5100_1_sendUDP(uint8 info[], uint8 length);
void W5100_1_readUDP();
uint8 W5100_1_dataReceived();
uint16 W5100_1_spiRead16( uint16);
uint8 W5100_1_spiRead8( uint16);
void W5100_1_spiSend16( uint8, uint16, uint8);
uint8 W5100_1_readPort0();
uint8 W5100_1_readPort1();
void W5100_1_setPort(uint8, uint8);
uint8 W5100_1_readIp0();
uint8 W5100_1_readIp1();
uint8 W5100_1_readIp2();
uint8 W5100_1_readIp3();
void W5100_1_setIp(uint8, uint8, uint8, uint8);

//[] END OF FILE